from collections import defaultdict
import csv
import time

class SchoolSearch:

    def __init__(self, file_name, file_encoding):
        self._file_name = file_name
        self._encoding = file_encoding  # 'latin1' for school_data.csv

        self._schools = list()
        self._schools_by_keywords = defaultdict(lambda: list())
        self._schools_by_city = defaultdict(lambda: list())
        self._schools_by_state = defaultdict(lambda: list())

    def get_keywords(self, key):
        return key.lower().split()

    def process_csv(self):
        with open(self._file_name, encoding=self._encoding) as csvfile:
            reader = csv.DictReader(csvfile)
            row_id = 0

            for row in reader:
                self._schools.append(row)

                for keyword in self.get_keywords(row['LCITY05']):
                    self._schools_by_city[keyword].append(row_id)

                for keyword in self.get_keywords(row['SCHNAM05']):
                    self._schools_by_keywords[keyword].append(row_id)

                self._schools_by_state[row['LSTATE05']].append(row_id)

                row_id += 1

    def search(self, search_text):
        keywords = self.get_keywords(search_text)

        ranking = defaultdict(lambda: 0)

        for keyword in keywords:
            for result in self._schools_by_state.get(keyword, []):
                ranking[result] += 100

            for result in self._schools_by_city.get(keyword, []):
                ranking[result] += 10

            for result in self._schools_by_keywords.get(keyword, []):
                ranking[result] += 1

        search_results = [(key, value) for key, value in ranking.items()]
        search_results.sort(key=lambda x: x[1], reverse=True)

        schools = [self._schools[result[0]] for result in search_results[:3]]

        return schools


school_search = SchoolSearch('school_data.csv', 'latin1')
school_search.process_csv()


def search_schools(search_text):
    time_start = time.time()
    search_results = school_search.search(search_text)
    time_end = time.time()

    print(f'Results for "{search_text}" (search took: {time_end - time_start:.3f}s)')

    for index, school in enumerate(search_results):
        print(f'{index+1}. {school["SCHNAM05"]}')
        print(f'{school["LCITY05"]}, {school["LSTATE05"]}')


if __name__ == '__main__':
    search_schools('elementary school highland park')
    print('-'*80)
    search_schools('jefferson belleville')
    print('-'*80)
    search_schools("riverside school 44")
    print('-'*80)
    search_schools("granada charter school")
    print('-'*80)
    search_schools("foley high alabama")
    print('-'*80)
    search_schools("KUSKOKWIM")
