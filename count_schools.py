from collections import defaultdict
import csv

class SchoolCounter:

    def __init__(self, file_name, file_encoding):
        self._file_name = file_name
        self._encoding = file_encoding  # 'latin1' for school_data.csv

        self._school_count = 0
        self._school_count_by_state = defaultdict(lambda: 0)
        self._school_count_by_metro_centric_locale = defaultdict(lambda: 0)
        self._school_count_by_city = defaultdict(lambda: 0)

    def process_csv(self):
        with open(self._file_name, encoding=self._encoding) as csvfile:
            reader = csv.DictReader(csvfile)
            for row in reader:

                self._school_count += 1

                self._school_count_by_state[row['LSTATE05']] += 1

                self._school_count_by_metro_centric_locale[row['MLOCALE']] += 1

                self._school_count_by_city[row['LCITY05']] += 1

    def print_total_schools(self):
        print(f'Total Schools: {self._school_count}')

    def print_schools_by_state(self):
        print('Schools by State:')

        for state, count in self._school_count_by_state.items():
            print(f'{state}: {count}')

    def print_schools_by_metro_centric_locale(self):
        print('Schools by Metro-centric locale:')

        for locale, count in self._school_count_by_metro_centric_locale.items():
            print(f'{locale}: {count}')

    def print_most_schools_city(self):
        max_count = 0
        max_count_city = None

        for city, count in self._school_count_by_city.items():
            if count > max_count:
                max_count = count
                max_count_city = city

        print(f'City with most schools: {max_count_city} ({max_count} schools)')


    def print_unique_cities_with_at_least_one_school(self):
        print(f'Unique cities with at least one school: {len(self._school_count_by_city)}')


def print_counts():
    school_counter = SchoolCounter('school_data.csv', 'latin1')

    school_counter.process_csv()

    school_counter.print_total_schools()
    school_counter.print_schools_by_state()
    school_counter.print_schools_by_metro_centric_locale()
    school_counter.print_most_schools_city()
    school_counter.print_unique_cities_with_at_least_one_school()


if __name__ == '__main__':
    print_counts()
